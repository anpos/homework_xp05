"""Fractal."""

from PIL import Image
from collections import namedtuple
from sys import float_info

Size = namedtuple('Size', 'width height')
Point = namedtuple('Point', 'x y')
Color = namedtuple('Color', 'r g b')


class ColorPoint:
    """ColorPoint struct."""

    __slots__ = ['weight', 'color']

    def __init__(self, weight: float, color: Color):
        """Strict types."""
        self.weight, self.color = weight, color


def linear_interpolate_color(v: float, vmin: float, vmax: float, color_list: list) -> Color:
    """Linearly calculate color."""
    # index_position = value_position / value_range * max_index
    index_position = (v - vmin) / (vmax - vmin) * (len(color_list) - 1)
    index, fraction = int(index_position) // 1, index_position % 1
    # while zero can have exact representation, any calculated value should be compared with epsilon - a value
    # that makes a difference
    if fraction < float_info.epsilon:
        return color_list[index]
    c1, c2 = color_list[index], color_list[index + 1]
    return Color(int(c1.r + fraction * (c2.r - c1.r)),
                 int(c1.g + fraction * (c2.g - c1.g)),
                 int(c1.b + fraction * (c2.b - c1.b)))


def linear_path_interpolate_color(v: float, vmin: float, vmax: float, color_path: list) -> Color:
    """Linearly calculate color with defined color path."""
    pre = 0
    normalized_value = (v - vmin) / (vmax - vmin)
    for cur in range(len(color_path)):
        if normalized_value >= color_path[cur].weight:
            pre = cur
        else:
            return linear_interpolate_color(normalized_value, color_path[pre].weight, color_path[cur].weight,
                                            [color_path[pre].color, color_path[cur].color])
    return color_path[pre].color


class Fractal:
    """Fractal class."""

    RESCALE_IMAGE = 1
    RESCALE_ZOOM = 2

    def __init__(self, size: tuple, scale: list, computation, rescale: int = 0, color_path: list = None):
        """Constructor.

        Arguments:
        size -- the size of the image as a tuple (x, y)
        scale -- the scale of x and y as a list of 2-tuple
                 [(minimum_x, minimum_y), (maximum_x, maximum_y)]
        computation -- the function used for computing pixel values as a function
        """
        self.image_size = Size(*size)
        self.scale_min = Point(*scale[0])
        self.scale_max = Point(*scale[1])
        self.scale_size = Size(self.scale_max.x - self.scale_min.x, self.scale_max.y - self.scale_min.y)
        self.computation = computation
        self.values = {}
        self.color_path = color_path
        if color_path is None:
            self.color_path = [
                ColorPoint(0.0000, Color(47, 0, 47)),
                ColorPoint(0.1000, Color(63, 0, 55)),
                ColorPoint(0.3000, Color(255, 0, 0)),
                ColorPoint(0.5000, Color(255, 127, 0)),
                ColorPoint(0.7000, Color(255, 255, 0)),
                ColorPoint(0.9999, Color(255, 255, 255)),
                ColorPoint(1.0000, Color(0, 0, 0))]
        if rescale == 0:
            return
        image_ratio = self.image_size.width / self.image_size.height
        scale_ratio = self.scale_size.width / self.scale_size.height
        if image_ratio > scale_ratio:
            if rescale == Fractal.RESCALE_IMAGE:
                self.image_size = Size(int(scale_ratio * self.image_size.height), self.image_size.height)
            elif rescale == Fractal.RESCALE_ZOOM:
                self.scale_size = Size(self.scale_size.width, self.scale_size.width / image_ratio)
        elif image_ratio < scale_ratio:
            if rescale == Fractal.RESCALE_IMAGE:
                self.image_size = Size(self.image_size.width, int(self.image_size.width / scale_ratio))
            elif rescale == Fractal.RESCALE_ZOOM:
                self.scale_size = Size(image_ratio * self.scale_size.height, self.scale_size.height)

    def on_scale(self, pixel_coord: Point) -> Point:
        """Convert pixel coordinate to scale."""
        return Point((pixel_coord.x / self.image_size.width) * self.scale_size.width + self.scale_min.x,
                     (pixel_coord.y / self.image_size.height) * self.scale_size.height + self.scale_min.y)

    def compute(self):
        """Create the fractal by computing every pixel value."""
        for y in range(self.image_size.height):
            for x in range(self.image_size.width):
                self.values[x, y] = self.computation(self.on_scale(Point(x, y)))

    def pixel_value(self, coord: tuple) -> int:
        """
        Return the number of iterations it took for the pixel to go out of bounds.

        Arguments:
        pixel -- the pixel coordinate (x, y)

        Returns:
        the number of iterations of computation it took to go out of bounds as integer.
        """
        return self.values[coord]

    def save_image(self, filename, iterations: int = 0):
        """
        Save the image to hard drive.

        Arguments:
        filename -- the file name to save the file to as a string.
        """
        img = Image.new('RGB', self.image_size)
        pixels = img.load()
        if iterations:
            for coord in self.values.keys():
                pixels[coord] = linear_path_interpolate_color(self.values[coord], 0, iterations, self.color_path)
        else:
            vmin, vmax = min(self.values.values()), max(self.values.values())
            for coord in self.values.keys():
                pixels[coord] = linear_path_interpolate_color(self.values[coord], vmin, vmax, self.color_path)
        img.save(filename)
        img.close()


if __name__ == "__main__":
    image_size = (1000, 1000)  # (3840, 2160)
    rescaling = Fractal.RESCALE_IMAGE
    computation_iterations = 512
    mandelbrot_start = 0
    julia_start = 0

    colors = [
        ColorPoint(0.0000, Color(255, 255, 255)),
        ColorPoint(0.0300, Color(255, 223, 223)),
        ColorPoint(0.3500, Color(255, 0, 0)),
        ColorPoint(0.7000, Color(63, 63, 63)),
        ColorPoint(0.9999, Color(1, 1, 1)),
        ColorPoint(1.0000, Color(0, 0, 0))]

    mandelbrot = [
        [Point(-0.117157469663600014665, 0.933429655865333333225),
         Point(-0.117152003624666681347, 0.933433814807999999880)],
        [Point(-1.154353899999999988931, 0.214774300000000000000),
         Point(-1.153865499999999988931, 0.215140600000000000000)],
        [Point(-1.402342086438888871513, -0.000799654591666666640),
         Point(-1.400271196280555538130, 0.000752354811111111175)],
        [Point(0.324192045303999974483, 0.490368234388611111115),
         Point(0.324227570380888863395, 0.490394878196277777799)],
        [Point(-0.049479977800000016386, 0.674681300433333333151),
         Point(-0.049463457144444460848, 0.674697821088888888689)],
        [Point(-2, -2), Point(2, 2)]
    ]

    julia = [
        ([Point(-1.5, -1), Point(1.5, 1)], complex(-0.835, -0.2321)),
        ([Point(-1.5, -1), Point(1.5, 1)], complex(-0.8, 0.156))
    ]

    def mandelbrot_computation(pixel: Point) -> int:
        """Mandelbrot."""
        c = complex(pixel.x, pixel.y)
        z = complex(0, 0)
        for n in range(computation_iterations):
            if abs(z) > 2.0:
                return n
            z = z * z + c
        return computation_iterations

    def julia_computation(pixel: Point) -> int:
        """Julia."""
        z = complex(pixel.x, pixel.y)
        c = julia_param
        for n in range(computation_iterations):
            if abs(z) > 2.0:
                return n
            z = z * z + c
        return computation_iterations

    for i in range(mandelbrot_start, len(mandelbrot)):
        print(f"Computing mandelbrot: {i}")
        frac = Fractal(image_size, mandelbrot[i], mandelbrot_computation, rescaling, colors)
        frac.compute()
        frac.save_image(f"./png/mandelbrot_{i}_{frac.image_size.width}x{frac.image_size.height}.png")

    for i in range(julia_start, len(julia)):
        print(f"Computing julia: {i}")
        julia_param = julia[i][1]
        frac = Fractal(image_size, julia[i][0], julia_computation, rescaling, colors)
        frac.compute()
        frac.save_image(f"./png/julia_{i}_{frac.image_size.width}x{frac.image_size.height}.png")
